const fs = require('fs');
const ledgerType = require('./ledgerType');

const blockDBPath = `${__dirname}/block`
const stateDBPath = `${__dirname}/state`

class Ledger{
    static set(type, data){
        let path = "";
        try {
            switch (type) {
                case ledgerType.block:
                    path = blockDBPath;
                break;

                case ledgerType.state:
                    path = stateDBPath;
                break;
            }
            fs.writeFileSync(path, JSON.stringify(data));
        } catch (error) {
          console.error(`Got an error trying to write in the file ${path} : ${error.message}`);
        }
    }

    static get(type){
        let path = "";
        try {
            switch (type) {
                case ledgerType.block:
                    path = blockDBPath;
                break;

                case ledgerType.state:
                    path = stateDBPath;
                break;
            }

            let data = fs.readFileSync(path, "utf8");
            return JSON.parse(data);
        } catch (error) {
          console.error(`Got an error trying to read the file ${path} : ${error.message}`);
        }
    }

    static init(){
        Ledger.set(ledgerType.state, {});
        Ledger.set(ledgerType.block, []);
    }
}

// Ledger.set(ledgerType.block, {hello: "hi"});
// Ledger.set(ledgerType.state, {hello: "hi"});

// console.log(Ledger.get(ledgerType.block));
// console.log(Ledger.get(ledgerType.state));

module.exports = Ledger;