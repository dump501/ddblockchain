const Ledger = require("./Ledger");
const ledgerType = require("./ledgerType");
const keys = require("../keys.json");

class StateDatabase{
    static setState(
        address, 
        data={
            balance : 0,
            body : "",
            nonce: 0,
            storage : {}
        }
    ){
        let state = Ledger.get(ledgerType.state);
        if(!state){
            state = {}
        }
        const existedAddresses = Object.keys(state);
        // if address doesn't exists in db we create one
        if(!existedAddresses.includes(address)){
            state[address] = {
                balance : 0,
                body : "",
                nonce: 0,
                storage : {}
            }
        }
        // update state
        state[address] = data;
        
        Ledger.set(ledgerType.state, state);
    }

    static getState(){
        let res = Ledger.get(ledgerType.state);
        return res;
    }
    /**
     * 
     * @param {*} address 
     * @returns {{balance, body, nonce, storage}}
     */
    static get(address){
        let state;
        state = Ledger.get(ledgerType.state);
        if(!state){
            state = {}
        }
        let addressState;
        const existedAddresses = Object.keys(state);
        // if address doesn't exists in db we create one
        if(!existedAddresses.includes(address)){
            addressState = {
                balance : 0,
                body : "",
                nonce: 0,
                storage : {}
            }
            // update state
            state[address] = addressState;
            // write
            Ledger.set(ledgerType.state, state);
        }
        addressState = state[address];

        return addressState;
    }
}

// StateDatabase.setState(keys.holder.public, {balance: 10000, body: "", nonce: 1, storage: {}});
// console.log(StateDatabase.getState());

module.exports = StateDatabase;