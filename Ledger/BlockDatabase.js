const Ledger = require("./Ledger");
const ledgerType = require("./ledgerType");
const keys = require("../keys.json");

class BlockDatabase{
    static addBlock(block){
        let blocks = Ledger.get(ledgerType.block);
        if(!blocks){
            blocks = [];
        }
        blocks.push(block);

        Ledger.set(ledgerType.block, blocks);
    }

    static getBlocks(){
        let blocks = Ledger.get(ledgerType.block);
        if(!blocks){
            blocks = [];
        }

        return blocks;
    }
    
}

// StateDatabase.setState(keys.holder.public, {balance: 10000, body: "", nonce: 1, storage: {}});
// console.log(StateDatabase.getState());

module.exports = BlockDatabase;