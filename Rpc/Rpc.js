const express = require('express');
const { ec } = require('../Core/Keys');
const Transaction = require('../Core/Transaction');
const { rpcLog } = require('../Debugger/Debugger');

function startRpc({port=5000, sendTransaction, keyPair}){
  
  const app = express();
  app.use(express.json());

  // app.post('/transaction/sign', (req, res)=>{
  //   let data = req.body.data;

  //   let transaction = data;
  //   Transaction.sign(transaction, keyPair);
  //   response(res, {message: "Transaction created successfully", data: transaction});

  // });

  app.post('/transaction/send_transaction', (req, res)=>{
    let data = req.body.data;

    let transaction = data;
    Transaction.sign(transaction, keyPair);
    sendTransaction(transaction);
    response(res, {message: "Transaction created successfully"});

  });

  app.listen(port, ()=>{
    console.log("RPC server is running on port :", port)
  })

  function response(res, data, status=200){
    res.status(status).send(data)
  }
}

module.exports = {startRpc}