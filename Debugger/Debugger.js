function rpcLog(data){
    if(process.env.ENABLE_RPC_LOG){
        console.log(" ===============>>>>> RPC LOG :: "+ data.text, data.content);
    } 
}

function nodeLog(data){
    if(process.env.ENABLE_NODE_LOG){
        console.log(" ===============>>>>> NODE LOG :: " + data.text, data.content );
    } 
}

module.exports = {rpcLog, nodeLog}