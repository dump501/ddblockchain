const messageType = {
    handShake: "TYPE_HANDSHAKE",
    createTransaction: "TYPE_CREATE_TRANSACTION",
    createBlock: "TYPE_REPLACE_CHAIN",
    requestCheck: "TYPE_REQUEST_CHECK",
    sendCheck: "TYPE_SEND_CHECK",
    sendChain: "TYPE_SEND_CHAIN",
    requestChain: "TYPE_REQUEST_CHAIN",
    requestInfo: "TYPE_REQUEST_INFO",
    sendInfo: "TYPE_SEND_INFO"
}

module.exports = messageType;