

// send message
function sendMessage(message, nodes){
    nodes.forEach(node => {
        node.socket.send(JSON.stringify(message));
    });
}

function produceMessage(type, data) {
    return { type, data }
}


module.exports = { sendMessage, produceMessage}