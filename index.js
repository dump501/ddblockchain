const options = require("./config.json")
const startServer = require("./Node/Node");
(async ()=> {
    await startServer(options);
})();