const keys = require("./keys.json");

const WS = require("ws");
const ddBlockchain = require("./ddBlockchain");
const { SHA256 } = require("./Core/HashHelper");
const Block = require("./Core/Block");
const Blockchain = require("./Core/Blockchain");

const PORT = process.env.PORT || 3001;
const PEERS = process.env.PEERS ? process.env.PEERS.split(",") : ["ws://localhost:3000"];
const MY_ADDRESS = process.env.MY_ADDRESS || "ws://localhost:3001";
let opened = [];
let connected = [];

const server = new WS.Server({port: PORT});

console.log("Listening on port", PORT);

// error handling
process.on("uncaughtException", err => console.log(err));

// connection listener
server.on("connection", async(socket, req)=>{
    // listen for messages
    socket.on("message", message => {
        let check = [];
        let checked = [];
        let checking = false;
        let tempChain = new Blockchain();

        // parse the message from a JSON into an object
        const _message = JSON.parse(message)

        switch(_message.type){
            case "TYPE_HANDSHAKE":
                const nodes = _message.data;

                nodes.forEach(node => {
                    connect(node)
                });

            break;

            case "TYPE_CREATE_TRANSACTION":
                const transaction = _message.data;
                ddBlockchain.addTransaction(transaction);

            break;
            
            case "TYPE_REPLACE_CHAIN":

                const [newBlock, newDiff] = _message.data;

                // check if the transactions exist in the pool by removing elts from transactions of 
                // the block if they exist in the pool
                // then, we use `theirTx.length === 0` to check if all the elmts are removed, meaning all tx
                // are in the pool
                const ourTx = [...ddBlockchain.transactions.map(tx => JSON.stringify(tx))];
                const theirTx = [...newBlock.data.filter(tx => tx.from !== keys.mint.public)
                    .map(tx => JSON.stringify(tx))];
                const n = theirTx.length;

                if(newBlock.prevHash !== ddBlockchain.getLastBlock().prevHash){
                    for(let i=0; i<n; i++){
                        const index = ourTx.indexOf(theirTx[0]);

                        if(index === -1) break;

                        ourTx.splice(index, 1);
                        theirTx.splice(0, 1);
                    }

                    if(
                        theirTx.length === 0 &&
                        SHA256(ddBlockchain.getLastBlock().hash + newBlock.timestamp + JSON.stringify(newBlock.data) + newBlock.nonce) === newBlock.hash &&
                        newBlock.hash.startsWith(Array(ddBlockchain.difficulty + 1).join("0")) &&
                        Block.hasValidTransactions(newBlock, ddBlockchain) &&
                        (parseInt(newBlock.timestamp) > parseInt(ddBlockchain.getLastBlock().timestamp) || ddBlockchain.getLastBlock().timestamp === "") &&
                        parseInt(newBlock.timestamp) < Date.now() &&
                        ddBlockchain.getLastBlock().hash === newBlock.prevHash &&
                        (newDiff + 1 === ddBlockchain.difficulty || newDiff - 1 === ddBlockchain.difficulty)
                    ) {
                        ddBlockchain.chain.push(newBlock);
                        ddBlockchain.difficulty = newDiff;
                        ddBlockchain.transactions = [...ourTx.map(tx => JSON.parse(tx))];
                    }
                } else if(!checked.includes(JSON.stringify([newBlock.prevHash, ddBlockchain.chain[ddBlockchain.length-2].timestamp || ""]))){
                    checked.push(JSON.stringify([ddBlockchain.getLastBlock().prevHash, ddBlockchain.chain[ddBlockchain.chain.length-2].timestamp || ""]));

                    const position = ddBlockchain.chain.length - 1;

                    checking = true;

                    sendMessage(produceMessage("TYPE_REQUEST_CHECK", MY_ADDRESS));

                    setTimeout(() => {
                        checking = false;

                        let mostAppeared = check[0];

                        check.forEach(group =>{
                            if(check.filter(_group => _group === group).length > check.filter(_group => _group === mostAppeared).length){
                                mostAppeared = group;
                            }
                        })

                        const group = JSON.parse(mostAppeared);

                        ddBlockchain.chain[position] = group[0];
                        ddBlockchain.transactions = [...group[1]];
                        ddBlockchain.difficulty = group[2];

                        check.splice(0, check.length);

                    }, 5000);
                }

            break;

            case "TYPE_REQUEST_CHECK":
                // find the address and send back the necessary data.
                opened.filter(node => node.address === _message.data)[0].socket.send(
                    JSON.stringify(produceMessage(
                        "TYPE_SEND_CHECK",
                        JSON.stringify([ddBlockchain.getLastBlock(), ddBlockchain.transactions, ddBlockchain.difficulty])
                    ))
                );

            break;

            case "TYPE_SEND_CHECK":
                // only push to check if checking is enabled
                if(checking) check.push(_message.data);

            break;

            case "TYPE_SEND_CHAIN":
                const {block, finished} = _message.data;

                if(!finished){
                    tempChain.chain.push(block);
                } else {
                    tempChain.chain.push(block);
                    if(Blockchain.isValid(tempChain)){
                        ddBlockchain.chain = tempChain.chain;
                    }
                    tempChain = new Blockchain();
                }

            break;

            case "TYPE_REQUEST_CHAIN":
                const socket = opened.filter(node => node.address === message.data)[0].socket;

                // send blocks continiously
                for(let i = 1; i<ddBlockchain.chain.length; i++){
                    socket.send(JSON.stringify(produceMessage(
                        "TYPE_SEND_CHAIN",
                        {
                            block: ddBlockchain.chain[i],
                            finished: i=== ddBlockchain.chain.length - 1
                        }
                    )));
                }
            
            break;

            case "TYPE_REQUEST_INFO":
                opened.filter(node => node.address === _message.data)[0].socket.send(
                    "TYPE_SEND_INFO",
                    [ddBlockchain.difficulty, ddBlockchain.transactions]
                );

            break;

            case "TYPE_SEND_INFO":
                [ddBlockchain.difficulty, ddBlockchain.transactions] = _message.data;
                
            break;

        }
    })
})

function produceMessage(type, data) {
    return { type, data }
}


// connection function
async function connect(address){
    // we will connect to the node if we haven't and we should not connect to ourself
    if(!connected.find(peerAddress => peerAddress === address) && address != MY_ADDRESS){
        const socket = new WS(address);

        socket.on("open", () => {
            // include our connected node's addresses into the message's body and send it.
            socket.send(JSON.stringify(produceMessage("TYPE_HANDSHAKE", [MY_ADDRESS, ...connected])))

            // give other nodes this one's address and ask them to connect
            opened.forEach(node => node.socket.send(JSON.stringify(produceMessage("TYPE_HANDSHAKE", [address]))));

            // if "opened" already contained the address, we will not push
            if(!opened.find(peer => peer.address === address) && address !== MY_ADDRESS){
                opened.push({socket, address});
            }

            // if "connected" already containded the address we will not connect
            if(!connected.find(peerAddress => peerAddress === address) && address !== MY_ADDRESS){
                connected.push(address);
            }

            // Two upper if statements exist because of the problem of asynchronous codes. Since they are running
            // concurrently, the first if statement can be passed easily, so there will be duplications.
            console.log("conneced with ", address);
        });

        socket.on("close", ()=>{
            opened.splice(connected.indexOf(address), 1);
            connected.splice(connected.indexOf(address), 1);
        })
    }
}

// connect to all prefixed peers
PEERS.forEach(peer => connect(peer));

function sendMessage(message){
    opened.forEach(node => {
        node.socket.send(JSON.stringify(message));
    });
}



// setTimeout(() => {
//     if (ddBlockchain.transactions.length !== 0) {
//         ddBlockchain.mineTransactions(publicKey);

//         sendMessage(produceMessage("TYPE_REPLACE_CHAIN", [
//             ddBlockchain.getLastBlock(),
//             ddBlockchain.difficulty
//         ]));
//     }
// }, 6500);

// setTimeout(() => {
// console.log(opened);
// console.log(ddBlockchain);
// }, 10000);