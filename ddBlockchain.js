
const Blockchain = require("./Core/Blockchain.js");
const keys = require("./keys.json");
const Ledger = require("./Ledger/Ledger.js");

Ledger.init();

const ddBlockchain = new Blockchain();

// ddBlockchain.addBlock(new Block(Date.now().toString(), {from: "Alice", to: "Bob", amount: 100}));
console.log("Blockchain created");
// console.log(ddBlockchain.getBalance(keys.holder.public));

/* const bobWallet = ec.genKeyPair();

// create a transaction
const transaction = new Transaction(HOLDER_PUBLIC_ADDRESS, bobWallet.getPublic("hex"), 100, 10);

// sign the transaction
transaction.sign(holderKeyPair);
// add transaction to the pool
ddBlockchain.addTransaction(transaction);
// mine transaction
ddBlockchain.mineTransactions(HOLDER_PUBLIC_ADDRESS);

console.log("blockchain's balance: ", ddBlockchain.getBalance(HOLDER_PUBLIC_ADDRESS));
console.log("bob's balance: ", ddBlockchain.getBalance(bobWallet.getPublic("hex"))); */

module.exports = ddBlockchain;