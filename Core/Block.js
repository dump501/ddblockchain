const { nodeLog } = require("../Debugger/Debugger");
const { SHA256 } = require("./HashHelper");
const { MINT_PUBLIC_ADDRESS } = require("./Keys");


class Block{
    constructor(timestamp = Date.now().toString(), data = []){
        this.timestamp = timestamp;
        this.data = data;
        this.hash = Block.getHash(this);
        this.prevHash = "";
        this.nonce = 0;
    }
    /**
     * get the block's hash
     * @returns the block's hash
     */
    static getHash(block){
        return SHA256(block.prevHash + block.timestamp + JSON.stringify(block.data) + block.nonce);
    }
    /**
     * mine a block
     * @param Number difficulty 
     */
    mine(difficulty){
        // loop until our hash starts with the string 0...0 with the length of <difficulty>
        while(!this.hash.startsWith(Array(difficulty+1).join("0"))){
            this.nonce++;

            // update the new hash
            this.hash = Block.getHash(this);
        }
    }
    /**
     * check if the block has valid transactions
     * @param {*} chain 
     * @returns true if the block's transactions are valid and false if not
     */
    static hasValidTransactions(block, chain){
        let gas = 0;
        let reward = 0;

        block.data.forEach(transaction => {
            if(transaction.from !== MINT_PUBLIC_ADDRESS){
                gas += transaction.gas;
            } else {
                reward = transaction.amount;
            }
        })

        let resp =(
            reward -gas === chain.reward &&
            block.data.every(transaction => transaction.isValid(transaction, chain)) &&
            block.data.filter(transaction => transaction.from === MINT_PUBLIC_ADDRESS).length === 1
        )
        nodeLog({text: "has valid transaction", content: resp});

        return resp;
    }
}


module.exports = Block;