const EC = require("elliptic").ec;
const ec = new EC("secp256k1");
// cons
const MINT_KEY_PAIR = ec.genKeyPair();
const MINT_PUBLIC_ADDRESS = MINT_KEY_PAIR.getPublic("hex");

const holderKeyPair = ec.genKeyPair();
const HOLDER_PUBLIC_ADDRESS = holderKeyPair.getPublic("hex");

module.exports = {ec}