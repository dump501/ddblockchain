const { SHA256 } = require("./HashHelper");
const keys = require("../keys.json");
const Blockchain = require("./Blockchain");

const EC = require("elliptic").ec;
const ec = new EC("secp256k1");
// cons

class Transaction{
    constructor(from, to, amount, gas=0){
        this.from = from;
        this.to = to;
        this.amount = amount;
        this.gas = gas;
    }
    /**
     * sign the transaction
     * @param {*} keyPair 
     */
    sign(keyPair){
        if(keyPair.getPublic("hex") === this.from){
            // sign the transaction
            this.signature = keyPair.sign(SHA256(this.from + this.to + this.amount + this.gas), "base64").toDER("hex");
        }
    }

    /**
     * sign a transaction
     * @param {*} transaction 
     * @param {*} keyPair 
     */
    static sign(transaction, keyPair){
        if(keyPair.getPublic("hex") === transaction.from){
            // sign the transaction
            transaction.signature = keyPair.sign(SHA256(transaction.from + transaction.to + transaction.amount + transaction.gas), "base64").toDER("hex");
            // return transaction;
        }
    }
    /**
     * check if a transaction is valid
     * @param {Transaction} tx 
     * @param {Blockchain} chain 
     * @returns true if the transaction is valid and false if not
     */
    static isValid(tx, chain){
        console.log("sender balance: ", chain.getBalance(tx.from));
        return (
            tx.from &&
            tx.to && 
            tx.amount &&
            // the balance of the sender is >= the tx amount or the tx sender is the chain (mint public address)
            (chain.getBalance(tx.from) >= tx.amount + tx.gas || tx.from === keys.mint.public) &&
            ec.keyFromPublic(tx.from, "hex").verify(SHA256(tx.from + tx.to + tx.amount + tx.gas), tx.signature)
        )
    }
}


module.exports = Transaction;