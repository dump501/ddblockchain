const crypto = require("crypto");
// sha256 hashing function
const SHA256 = message => crypto.createHash("sha256").update(message).digest("hex");

module.exports = {SHA256};