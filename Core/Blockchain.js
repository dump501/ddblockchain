const Block = require("./Block.js");
const { ec } = require("./Keys.js");
const keys = require("../keys.json");
const Transaction = require("./Transaction.js");
const { nodeLog } = require("../Debugger/Debugger.js");
const StateDatabase = require("../Ledger/StateDatabase.js");
const BlockDatabase = require("../Ledger/BlockDatabase.js");



let initState = async (transaction)=>{
    let state = await StateDatabase.getState();
    if(!state){
        state = {}
    }
    const existedAddresses = Object.keys(state);
    let holderState;
    if(!existedAddresses.includes(transaction.to)){
        holderState = {
            balance : transaction.amount.toString(),
            body : "",
            nonce: 0,
            storage : {}
        }
    } else{
        holderState = state[transaction.to];
    }
    holderState.balance = transaction.amount.toString();
    // state[holderAddress] = holderState;
    await StateDatabase.setState(transaction.to, holderState);
}

class Blockchain{
    constructor(){
        const initialSupply = 10000000000;
        // release 10 000 coins and mint to to holder
        const initialCoinRelease = new Transaction(keys.mint.public, keys.holder.public, initialSupply, 0)
        // update state
        initState(initialCoinRelease);

        // store to block db;
        BlockDatabase.addBlock(new Block(Date.now().toString(), [initialCoinRelease]));
        console.log(BlockDatabase.getBlocks());

        // contains all the blocks. it contains the genesis block
        this.chain = [new Block(Date.now().toString(), [initialCoinRelease])];
        this.difficulty = 1;
        // Estimate that the time take for a block to be added on chain is 30 seconds
        this.blockTime = 30000;
        this.transactions = [];
        this.reward = 297;
    }

    /**
     * get the latest block
     * @returns the latest block
     */
    getLastBlock(){
        return this.chain[this.chain.length -1];
    }

    /**
     * adds a new block to the blockchain
     * @param Block block 
     */
    addBlock(block){
        // hash of the previous block
        block.prevHash = this.getLastBlock().hash;
        // set the current block hash
        block.hash = Block.getHash(block);
        // mine the block
        block.mine(this.difficulty);
        // Object.freeze ensures the immutability in our code
        this.chain.push(Object.freeze(block));

        // update the difficulty
        this.difficulty += Date.now() - parseInt(this.getLastBlock().timestamp) < this.blockTime ? 1 : -1;
    }

    /**
     * check if the blockchain is valid
     * @param Blockchain blockchain 
     * @returns wether the blockchain is valid or not
     */
    static isValid(blockchain){
        // NB: we start at 1 because there is nothing before the genesisi block
        for(let i=1; i<blockchain.chain.length; i++){
            const currentBlock = blockchain[i];
            const prevBlock = blockchain[i-1];

            // check validation
            if(
                currentBlock.hash !== currentBlock.getHash() || 
                prevBlock.hash !== currentBlock.prevHash ||
                !currentBlock.hasValidTransactions(blockchain)
            ){
                return false;
            }
        }

        return true;
    }

    /**
     * add a transaction
     * @param {*} transaction 
     */
    static addTransaction(chain, transaction){
        if(Transaction.isValid(transaction, chain)){
            nodeLog({text: "Transaction added to pool", content: transaction});
            chain.transactions.push(transaction);
        }
    }

    mineTransactions(rewardAddress){
        let gas = 0;

        this.transactions.forEach(transaction => {
            gas += transaction.gas;
        })

        // create mint transaction for reward.
        const rewardTransaction = new Transaction(keys.mint.public, rewardAddress, this.reward + gas);
        rewardTransaction.sign(ec.keyFromPrivate(keys.mint.private, "hex"));

         // Prevent people from minting coins and mine the minting transaction.
        if (this.transactions.length !== 0){
            // add reward transaction in the pool
            this.addBlock(new Block(
                Date.now().toString(), 
                [rewardTransaction, ...this.transactions]
            ));

            console.log("++++++++ txx", [rewardTransaction, ...this.transactions]);
            
            // update the state db
            for (const tx of [rewardTransaction, ...this.transactions]) {
                // if the sender is not the mint address ei. mining reward transaction
                if(tx.from !== keys.mint.public){
                    console.log("normal tx");
                    let senderState = StateDatabase.get(tx.from);
                    let receiverState = StateDatabase.get(tx.to);

                    // remove the amount and gas from sender balance
                    console.log("sender state", senderState);
                    senderState.balance = (BigInt(senderState.balance) - BigInt(tx.amount + tx.gas)).toString();
                    console.log("sender state", senderState);
    
                    //add the amount to the receiver
                    console.log("receiver state", receiverState);
                    receiverState.balance = (BigInt(receiverState.balance) + BigInt(tx.amount)).toString();
                    console.log("receiver state", receiverState);

                    // sync
                    StateDatabase.setState(tx.from, senderState);
                    StateDatabase.setState(tx.to, receiverState);

                    console.log("normal tx end");

                } else {
                    console.log("mint tx");
                    // it is a mining reward tx
                    let receiverState = StateDatabase.get(tx.to);

                    console.log("receiver state", receiverState);
                    receiverState.balance = (BigInt(receiverState.balance) + BigInt(tx.amount)).toString();
                    console.log("receiver state", receiverState);
                    // sync
                    StateDatabase.setState(tx.to, receiverState);

                    console.log("mint tx end");
                }
            }

            // update block db
            BlockDatabase.addBlock(this.getLastBlock());
            console.log(BlockDatabase.getBlocks());

        }

        this.transactions = [];
        // console.log("++++++++++++++ chain",this.chain);
        // console.log("++++++++++++++ chain",this.getLastBlock().data[1]);
    }

    /**
     * get the balance of an address
     * @param {*} address 
     * @returns {String} the balance of the given address
     */
    getBalance(address){
        let balance =0;
        let state = StateDatabase.getState();
        const existedAddresses = Object.keys(state);
        if(!existedAddresses.includes(address)){
            return balance;
        }
        balance = state[address].balance;
        return balance;
    }

    /**
     * get the balance of an address
     * @param {*} address 
     * @returns the balance of the given address
     */
    getBalanceOld(address){
        let balance = 0;

        this.chain.forEach(block => {
            block.data.forEach(transaction => {
                // if you are the sender, the money dicrease
                if(transaction.from === address){
                    balance -= transaction.amount;
                    balance -= transaction.gas;
                }

                // if you are the receiver, the money increase
                if(transaction.to === address){
                    balance += transaction.amount;
                }
            })
        })

        return balance;
    }
}

module.exports = Blockchain;