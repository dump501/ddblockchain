const { SHA256 } = require("./Core/HashHelper");

const EC = require("elliptic").ec;
const ec = new EC("secp256k1");

const holderKeyPair = ec.genKeyPair();

console.log("public: ", holderKeyPair.getPublic("hex"));
console.log("private: ", holderKeyPair.getPrivate("hex"));
console.log("address: ", SHA256(holderKeyPair.getPublic("hex")));
